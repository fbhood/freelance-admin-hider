=== Freelance Admin Hider === 
Contributors: fabsere
Donate link: https://fabiopacifici.com/product/open-source-donate/
Tags: admin, tools
Requires at least: 5.4
Tested up to: 5.6
Stable tag: 0.3
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin hides menu items form the WordPress Admin Panel to help freelancers control which menu item is 
visible to a client.


== Description ==

The Freelance Admin Hider adds a new set on settings under the general settings page in the admin panel 
to allow freelancers to control which menu item the client sees 
in the admin panel when they log in.

The plugin hides specific pages and redirects the user back to the main
admin page if he tries to visit directly one of the hidden pages.

This way the freelance has full control over themes, plugins, settings and other
critical pages.

The plugin lets only the admin with an ID of 1 view these pages 
in a WordPress multisite installation it will be the super admin of the network.
 
Features of the plugin:
Under settings > general you can select to remove all the following items from the 
menu using a check box.

- Removes the Dashboard menu item
- Removes the Posts menu item
- Removes the Media menu item
- Removes the Pages menu item
- Removes teh Comments menu item
- Removes the Appearance menu item
- Removes the Plugins menu item
- Removes the Users menu item
- Removes the Tools menu item
- Removes the Settings menu item
- Remove theme editor menu
- Remove plugin editor menu

You can also remove from the top tools bar the following elements

- Removes the WordPress Logo
- Removes the updates menu

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `Freelance Admin Hider` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== How it Works ==

[youtube https://youtu.be/cU36Mw8NLhY]

== Frequently Asked Questions ==

= Does it work with any user? =

The plugin hides form any user even other administrators the menu items selected.
To access the plugin features you must be the first user registered to the site.
Which means that you must be the user with an ID of 1. 

== Screenshots ==

1. Admin Panel: Plugins Settings under General


== Changelog ==

= 0.3 = 
Release date: 3 January 2021
Updated readme.txt file with the following changes:
- Changed Tested up to from WordPress 5.4 to 5.6
- Changed required PHP version from 7.2 to 7.4
- Updated release number from 0.2 to 0.3

= 0.2 =
Release date: 18 August 2020
- Remove unused code inside teh freelance-admin-hider file.
- update the version to 0.2 in the plugin readme.txt file. 
- update the tested up to 5.5 in the plugin readme.txt file. 

= 0.1 =
First Stable Release

== Upgrade Notice ==

= 0.1 =
First Stable Release

== Compatible with any WordPress ==

This plugin is fully compatible with any WordPress theme.
