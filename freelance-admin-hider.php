<?php

/**
 * 
 * Plugin Name: Freelance Admin hider
 * Description: Controls which admin menu item to show
 * Author: Fabio Pacifici 
 * Text Domain: freelance-admin-hider
 * Version: 0.2
 * Author URI: https://fabiopacifici.com
 * Plugin URI: https://plugins.fabiopacifici.com/freelance-admin-hider
 * 
 */

if (!function_exists('add_action')) {

    die("Hi there is not much I can do when run directly! Go Vegan!");
}


//Setup
define('ADMIN_HIDER_PLUGIN_URL', __FILE__);
define('ADMIN_HIDER_PLUGIN_DIR', __DIR__);
require(ADMIN_HIDER_PLUGIN_DIR . "/functions.php");
require(ADMIN_HIDER_PLUGIN_DIR . "/settings_page.php");

add_action('admin_init', 'fpah_settings_api_init');

# Remove admin menus

function fpah_is_not_admin()
{
    /** Triggers the actions to remove elements form the admin panel  */
    $admin_id  = get_current_user_id();
    if ($admin_id != 1) {
        add_action('admin_menu', 'fpah_remove_menus');
        add_action('admin_init', 'fpah_remove_theme_editor');
        add_action('wp_before_admin_bar_render', 'fpah_remove_toolbar_icons');
    }
}
add_action('init', 'fpah_is_not_admin');
