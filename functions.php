 <?php
    function fpah_remove_menus()
    {
        /* Removes the Dashboard menu item */
        if (get_option('fpah_setting_hider_dashboard')) {
            remove_menu_page('index.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/index.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }

        /* Removes the Posts menu item */
        if (get_option('fpah_setting_hider_posts') == 1) {
            remove_menu_page('edit.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/edit.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }
        /* Removes the Media menu item */
        if (get_option('fpah_setting_hider_media') == 1) {
            remove_menu_page('upload.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/upload.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }

        /* Removes the Pages menu item */
        if (get_option('fpah_setting_hider_pages') == 1) {
            remove_menu_page('edit.php?post_type=page');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/edit.php?post_type=page") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }
        /* Removes teh Comments menu item*/
        if (get_option('fpah_setting_hider_comments') == 1) {
            remove_menu_page('edit-comments.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/edit-comments.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }
        /* Removes the Appearance menu item*/
        if (get_option('fpah_setting_hider_appearance') == 1) {
            remove_menu_page('themes.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/themes.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }
        /* Removes the Plugins menu item */
        if (get_option('fpah_setting_hider_plugins') == 1) {

            remove_menu_page('plugins.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/plugins.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }

        /* Removes the Users menu item*/
        if (get_option('fpah_setting_hider_users') == 1) {
            remove_menu_page('users.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/users.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }
        /* Removes the Tools menu item */
        if (get_option('fpah_setting_hider_tools') == 1) {
            remove_menu_page('tools.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/tools.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }
        /* Removes the Settings menu item */
        if (get_option('fpah_setting_hider_settings') == 1) {
            remove_menu_page('options-general.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/options-general.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }
    }


    # Remove theme editor menu

    function fpah_remove_theme_editor()
    {
        if (get_option('fpah_setting_hider_theme_editor') == 1) {

            remove_submenu_page('themes.php', 'theme-editor.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/theme-editor.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }

        if (get_option('fpah_setting_hider_plugins_editor') == 1) {
            remove_submenu_page('plugins.php', 'plugin-editor.php');
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/plugin-editor.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }
    }


    function fpah_remove_toolbar_icons($wp_admin_bar)
    {
        /** removes elements from the wp tool bar  */

        global $wp_admin_bar;

        if (get_option('fpah_setting_hider_wp_toolbar_logo') == 1) {
            $wp_admin_bar->remove_menu("wp-logo");
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/about.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }

        if (get_option("fpah_setting_hider_wp_toolbar_updates")) {
            $wp_admin_bar->remove_menu("updates");
            if ($_SERVER['REQUEST_URI'] == "/wp-admin/update-core.php") {
                $admin = home_url() . "/wp-admin";
                header('Location:' . $admin);
                exit();
            }
        }
    }
