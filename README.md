# Admin Hider

Removes a series of menus from the WordPress admin panel.

This plugin aims to allow freelancers to remove from the client's websites all menu Items that are not supposed to be
managed by the client like themes, plugins and settings or unnecessary items.

By default only the admin user with an ID of 1 can see these menus
when the settings are checked in the admin panel under settings > general.

## Works with the following menu items:

- Removes the Dashboard menu item
- Removes the Posts menu item
- Removes the Media menu item
- Removes the Pages menu item
- Removes teh Comments menu item
- Removes the Appearance menu item
- Removes the Plugins menu item
- Removes the Users menu item
- Removes the Tools menu item
- Removes the Settings menu item
- Remove theme editor menu
- Remove plugin editor menu

## ToolBar Removes

Removes from the top tools bar the following elements

- Removes the WordPress Logo
- Removes the updates menu
