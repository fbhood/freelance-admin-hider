<?php


// ------------------------------------------------------------------
// Add all your sections, fields and settings during admin_init
// ------------------------------------------------------------------
// https://codex.wordpress.org/Settings_API

function fpah_settings_api_init()
{
    // Add the section to general settings so we can add our fields to it.
    add_settings_section(
        'fpah_setting_section',
        'Freelancer Admin Menu Hider',
        'fpah_setting_section_callback_function',
        'general'
    );

    // Add the field with the names and function to use for our new settings, 
    // put it in our new section.
    $menus = ["dashboard", "posts", "media", "pages", "comments", "appearance", "plugins", "users", "tools", "settings", "theme_editor", "plugins_editor", "wp_toolbar_logo", "wp_toolbar_updates"]; //, "theme_sniffer", "admin_style"

    foreach ($menus as $value) {

        add_settings_field(
            'fpah_setting_hider_' . $value,
            'Hide ' . $value . ' Menu Item',
            'fpah_setting_callback_' . $value,
            'general',
            'fpah_setting_section'
        );
        register_setting('general', 'fpah_setting_hider_' . $value);
    }
} // fpah_settings_api_init()



// ------------------------------------------------------------------
// Settings section callback function
// ------------------------------------------------------------------
//
// This function is needed if we added a new section. This function 
// will be run at the start of our section
//

function fpah_setting_section_callback_function()
{
    echo '<p>Select the admin menu items to hide. The items will be hidden for all users 
    except the first user in the database, that should be the site administrator or the super 
    admin in a multi-site installation.</p>';
}

// ------------------------------------------------------------------
// Callback functions for our admin menu hider settings
// ------------------------------------------------------------------
//
// creates a checkbox true/false option.
//


function fpah_setting_callback_dashboard()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_dashboard" id="fpah_setting_hider_dashboard" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_dashboard'), false) . ' /> Hide Dashboard Menu</div>';
}
function fpah_setting_callback_posts()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_posts" id="fpah_setting_hider_posts" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_posts'), false) . ' /> Hide Posts Menu</div>';
}
function fpah_setting_callback_media()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_media" id="fpah_setting_hider_media" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_media'), false) . ' /> Hide Media Menu</div>';
}
function fpah_setting_callback_pages()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_pages" id="fpah_setting_hider_pages" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_pages'), false) . ' /> Hide Pages Menu</div>';
}
function fpah_setting_callback_comments()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_comments" id="fpah_setting_hider_comments" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_comments'), false) . ' /> Hide Comments Menu</div>';
}
function fpah_setting_callback_appearance()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_appearance" id="fpah_setting_hider_appearance" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_appearance'), false) . ' /> Hide Appearance Menu</div>';
}
function fpah_setting_callback_plugins()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_plugins" id="fpah_setting_hider_plugins" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_plugins'), false) . ' /> Hide plugins Menu</div>';
}
function fpah_setting_callback_users()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_users" id="fpah_setting_hider_users" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_users'), false) . ' /> Hide users Menu</div>';
}
function fpah_setting_callback_tools()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_tools" id="fpah_setting_hider_tools" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_tools'), false) . ' /> Hide tools Menu</div>';
}
function fpah_setting_callback_settings()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_settings" id="fpah_setting_hider_settings" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_settings'), false) . ' /> Hide Settings Menu</div>';
}
function fpah_setting_callback_theme_editor()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_theme_editor" id="fpah_setting_hider_theme_editor" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_theme_editor'), false) . ' /> Hide Theme Editor Subpage</div>';
}
function fpah_setting_callback_plugins_editor()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_plugins_editor" id="fpah_setting_hider_plugins_editor" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_plugins_editor'), false) . ' /> Hide Plugins Editor Subpage</div>';
}

function fpah_setting_callback_wp_toolbar_logo()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_wp_toolbar_logo" id="fpah_setting_hider_wp_toolbar_logo" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_wp_toolbar_logo'), false) . ' /> Hide WordPress logo from the top tools bar</div>';
}
function fpah_setting_callback_wp_toolbar_updates()
{
    echo '<div class="form-group"><input name="fpah_setting_hider_wp_toolbar_updates" id="fpah_setting_hider_wp_toolbar_updates" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_wp_toolbar_updates'), false) . ' /> Hide The update icon from the top tools bar</div>';
}


# TODO: Remove code below these menu items should not be active in production.
/** Checks if the biodev-wp-tools is active before
 * hiding the theme sniffer and admin styles menu items.
 * 
 */
/* if (is_plugin_active('biodev-wp-tools/index.php')) {
    function fpah_setting_callback_theme_sniffer()
    {
        echo '<div class="form-group"><input name="fpah_setting_hider_theme_sniffer" id="fpah_setting_hider_theme_sniffer" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_theme_sniffer'), false) . ' /> Hide Theme Sniffer Menu</div>';
    }
    function fpah_setting_callback_admin_style()
    {
        echo '<div class="form-group"><input name="fpah_setting_hider_admin_style" id="fpah_setting_hider_admin_style" type="checkbox" value="1" class="code" ' . checked(1, get_option('fpah_setting_hider_admin_style'), false) . ' /> Hide Admin Style Menu </div>';
    }
} */
